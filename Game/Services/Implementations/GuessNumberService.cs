﻿using Game.Services.Abstract;

namespace Game.Services.Implementations
{
    /// <summary>
    /// SRP, потому что изменения в данный класс будут вноситься, только если otus дополнит задание
    /// OCP, открыт для расширения, класс реализует интерфейс, поэтому он неизменно реализует все методы этого интерфейса и это нельзя изменить не изменив сам интерфейс,
    /// но можно расширить функциональность класса реализовав еще один интерфейс
    /// LSP, неприменим
    /// ISP, минимальный интерфейс, состоящий из одного метода играть в игру угадай число
    /// </summary>
    /// <returns></returns>
    public class GuessNumberService : IGuessNumberService
    {
        public void PlayTheGame(int numberFrom, int numberTo, int tryCount)
        {
            //решил, что лучше пусть игра выправит кривого пользователя
            if(numberTo < numberFrom)
            {
                int tempNumberFrom = numberFrom;
                numberFrom = numberTo;
                numberTo = tempNumberFrom;
            }
            int thoughtNumber = new Random().Next(numberFrom, numberTo);
            int playerNumber;
            Console.WriteLine($"Я загадал, начинай угадывать\n");
            while (!(int.TryParse(Console.ReadLine(), out playerNumber) && playerNumber == thoughtNumber))
            {
                tryCount--;
                if(tryCount <= 0) { 
                    break; 
                }
                bool isMore = thoughtNumber > playerNumber;
                Console.WriteLine($"Я загадал число {(isMore ? "больше" : "меньше")}\n");
            }
            if (tryCount > 0)
            {
                Console.WriteLine("Поздравляю с победой!\n");
            }
            else
            {
                Console.WriteLine("Вы проиграли!\n");
            }
            Console.WriteLine("Игра закончена\n");
        }
    }
}
