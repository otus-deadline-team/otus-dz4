﻿namespace Game.Services.Abstract
{
    public interface IGuessNumberService
    {
        /// <summary>
        /// Запускает игру угадай число
        /// </summary>
        /// <param name="numberFrom"></param>
        /// <param name="numberTo"></param>
        void PlayTheGame(int numberFrom, int numberTo, int tryCount);
    }
}
