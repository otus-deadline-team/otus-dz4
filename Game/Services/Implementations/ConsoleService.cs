﻿using Game.Services.Abstract;

namespace Game.Services.Implementations
{
    /// <summary>
    /// SRP, потому что изменения в данный класс будут вноситься, только если otus дополнит задание
    /// OCP, открыт для расширения, класс реализует интерфейс, поэтому он неизменно реализует все методы этого интерфейса и это нельзя изменить не изменив сам интерфейс,
    /// но можно расширить функциональность класса реализовав еще один интерфейс
    /// LSP, неприменим
    /// DIP, зависит от абстракции IGuessNumberService, а не от конкретной ее реализации GuessNumberService
    /// </summary>
    /// <returns></returns>
    public class ConsoleService : IConsoleService
    {
        private readonly bool _work = true;
        private readonly string badIntTryParseMessage = "Вводить можно только целое число, попробуй снова";
        private readonly IGuessNumberService _guessNumberService;
        bool firstStart = true;
        public ConsoleService(IGuessNumberService guessNumberService)
        {
            _guessNumberService = guessNumberService;
        }

        public async Task Start()
        {
            while (_work)
            {
                Console.WriteLine($"{(firstStart ? "Привет, давай сыграем в игру \"угадай число\"?" : "Сыграем еще раз? :)")} \n" +
                    "start - играть\n" +
                    "exit - для выхода из программы\n");

                var command = Console.ReadLine().ToLower();

                if (command == "exit")
                    break;

                if (command == "start")
                {
                    int numberFrom;
                    int numberTo;
                    int tryCount;
                    
                    Console.WriteLine("Укажи с какого числа я могу загадывать число:");
                    while (!int.TryParse(Console.ReadLine(), out numberFrom))
                    {
                        Console.WriteLine(badIntTryParseMessage);
                    }

                    Console.WriteLine("Принято, укажи по какое число я могу загадывать число:\n");
                    while (!int.TryParse(Console.ReadLine(), out numberTo))
                    {
                        Console.WriteLine(badIntTryParseMessage);
                    }

                    Console.WriteLine("Принято, сколько попыток мне тебе дать?:\n");
                    while (!(int.TryParse(Console.ReadLine(), out tryCount) && tryCount > 0))
                    {
                        Console.WriteLine(!(tryCount > 0) ? "Количество попыток должно быть больше 0. \n Как ты собираешься выигрывать у меня? :) \n Попробуй снова \n Сколько попыток тебе дать?" : badIntTryParseMessage);
                    }
                    firstStart = false;
                    _guessNumberService.PlayTheGame(numberFrom, numberTo, tryCount);
                }
                else
                {
                    Console.WriteLine("Команда не распознана");
                }
            }
        }

    }
}
