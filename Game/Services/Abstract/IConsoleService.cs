﻿namespace Game.Services.Abstract
{
    public interface IConsoleService
    {
        /// <summary>
        /// Запуск консоли для сбора информации о настройках
        /// </summary>
        /// <returns></returns>
        public Task Start();
    }
}
