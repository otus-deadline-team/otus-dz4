﻿using Game.Services.Abstract;
using Game.Services.Implementations;
using Microsoft.Extensions.DependencyInjection;

static class Program
{
    static async Task Main(string[] args)
    {
        var provider = Configure();
        var console = provider.GetService<IConsoleService>();
        await console.Start();
    }

    private static IServiceProvider Configure()
    {
        var services = new ServiceCollection();
        services.AddScoped<IConsoleService, ConsoleService>();
        services.AddScoped<IGuessNumberService, GuessNumberService>();

        return services.BuildServiceProvider();
    }
}